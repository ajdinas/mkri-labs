package lab2;

import java.util.ArrayList ;
import com.google.gson.GsonBuilder ;

public class Blockchain {
	
	public static ArrayList<Block> blockchain = new ArrayList<Block>();
	public static int difficulty = 5;
	
	public static void main(String[] args) {
		//prida bloky do blockchainu ve forme ArrayListu :
		blockchain.add(new Block("Ahoj, ja jsem prvni blok" , "0"));
		System.out.println("Tezim blok 1 . . .");
		blockchain.get(0). mineBlock(difficulty);
		blockchain.add(new Block("Ja jsem druhy", blockchain.get(blockchain.size()-1).hash));
		System.out.println("Tezim blok 2 . . .");
		blockchain.get(1).mineBlock(difficulty);
		blockchain.add(new Block("A ja treti" , blockchain.get(blockchain.size()-1).hash));
		System.out.println("Tezim blok 3 . . .");
		blockchain.get(2).mineBlock(difficulty);
		System.out.println("\nBlockchain je platny: " + isChainValid ());
		String blockchainJson = new GsonBuilder().setPrettyPrinting().create().toJson(blockchain);
		System.out.println("\nBlockchain:");
		System.out.println(blockchainJson);
	}
	
	public static Boolean isChainValid () {
		Block currentBlock ;
		Block previousBlock ;
		
		//projde cely blockchain a zkontorluje hashe
		for (int i =1; i < blockchain.size(); i++) {
			currentBlock = blockchain.get(i );
			previousBlock = blockchain.get(i -1);
			//porovna ulozeny hash a vypocitany hash

			if (! currentBlock.hash.equals(currentBlock.calculateHash()) ){
				System.out.println("Hash tohoto bloku je odlisny");
				return false ;
			}
			//porovna predchozi hash se skutecnym predchozim hashem
			if (! previousBlock.hash.equals(currentBlock.previousHash ) ) {
				System.out.println("Hash predchoziho bloku je odlisny");
				return false ;
			}
		}
		return true ;
	}
}