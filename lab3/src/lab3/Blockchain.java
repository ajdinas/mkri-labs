package lab3;

import java.security.Security;
import java.util.*;

public class Blockchain {

	public static ArrayList<Block> blockchain = new ArrayList<Block>();
	public static HashMap<String,TransactionOutput> UTXOs = new HashMap<String,TransactionOutput>();

	public static int difficulty = 3;
	public static float minimumTransaction = 0.1f;
	public static Wallet walletA;
	public static Wallet walletB;
	public static Transaction genesisTransaction;

	public static void main(String[] args) {
		//prida bloky do blockchainu
		Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider()); // Nastavi Bouncey castle jako Security Provider

		// Vytvoreni penezenek
		walletA = new Wallet();
		walletB = new Wallet();
		Wallet coinbase = new Wallet();

		//vytvori pocateni transakci která posle 100 minci do walletA
		genesisTransaction = new Transaction(coinbase.publicKey, walletA.publicKey, 100f, null);
		genesisTransaction.generateSignature(coinbase.privateKey);	 //manualne podepise pocatecni blok
		genesisTransaction.transactionId = "0"; //manualne nastavi id transakce
		genesisTransaction.outputs.add(new TransactionOutput(genesisTransaction.reciepient, genesisTransaction.value, genesisTransaction.transactionId)); //manualne nastavi hodnotu Outputs
		UTXOs.put(genesisTransaction.outputs.get(0).id, genesisTransaction.outputs.get(0)); //je dulezite ulozit první transakci do seznamu UTXO

		System.out.println("Vytvareni a tezeni pocatecniho bloku... ");
		Block genesis = new Block("0");
		genesis.addTransaction(genesisTransaction);
		addBlock(genesis);

		//testovani
		Block block1 = new Block(genesis.hash);
		System.out.println("\nStav penezenky walletA: " + walletA.getBalance());
		System.out.println("\nWalletA odesila 40 minci peněžence WalletB...");
		block1.addTransaction(walletA.sendFunds(walletB.publicKey, 40f));
		addBlock(block1);
		System.out.println("\nStav WalletA: " + walletA.getBalance());
		System.out.println("Stav WalletB: " + walletB.getBalance());

		Block block2 = new Block(block1.hash);
		System.out.println("\nWalletA zkousi odeslat 1000 minci, které ale nema...");
		block2.addTransaction(walletA.sendFunds(walletB.publicKey, 1000f));
		addBlock(block2);
		System.out.println("\nStav WalletA: " + walletA.getBalance());
		System.out.println("Stav WalletB: " + walletB.getBalance());

		Block block3 = new Block(block2.hash);
		System.out.println("\nWalletB odesila 20 minci penezence WalletA...");
		block3.addTransaction(walletB.sendFunds( walletA.publicKey, 20));
		System.out.println("\nStav WalletA: " + walletA.getBalance());
		System.out.println("Stav WalletB: " + walletB.getBalance());

		isChainValid();

	}

	public static Boolean isChainValid() {
		Block currentBlock;
		Block previousBlock;
		String hashTarget = new String(new char[difficulty]).replace('\0', '0');
		HashMap<String,TransactionOutput> tempUTXOs = new HashMap<String,TransactionOutput>(); // docasny pracovni seznam neutracenych transakci v danem bloku
		tempUTXOs.put(genesisTransaction.outputs.get(0).id, genesisTransaction.outputs.get(0));

		// projde cely blockchain a zkontorluje hashe
		for(int i=1; i < blockchain.size(); i++) {

			currentBlock = blockchain.get(i);
			previousBlock = blockchain.get(i-1);
			// porovna ulozeny hash a vypocitany hash:
			if(!currentBlock.hash.equals(currentBlock.calculateHash()) ){
				System.out.println("#Hash tohoto bloku je odlisny ");
				return false;
			}
			// porovna predchozi hash se skutecnym predchozim hashem
			if(!previousBlock.hash.equals(currentBlock.previousHash) ) {
				System.out.println("#Hash predchoziho bloku je odlisny ");
				return false;
			}
			//Kontrola, zda je hash vyresen
			if(!currentBlock.hash.substring( 0, difficulty).equals(hashTarget)) {
				System.out.println("#Tento blok nebyl vytezen");
				return false;
			}

			//projde všechny transakce blockchainu:
			TransactionOutput tempOutput;
			for(int t=0; t <currentBlock.transactions.size(); t++) {
				Transaction currentTransaction = currentBlock.transactions.get(t);

				if(!currentTransaction.verifiySignature()) {
					System.out.println("#Podpis transakce(" + t + ") je platny");
					return false;
				}
				if(currentTransaction.getInputsValue() != currentTransaction.getOutputsValue()) {
					System.out.println("#Transakce(" + t + ") nemá shodne vstupy a vystupy");
					return false;
				}

				for(TransactionInput input: currentTransaction.inputs) {
					tempOutput = tempUTXOs.get(input.transactionOutputId);

					if(tempOutput == null) {
						System.out.println("#Vstupy transakce(" + t + ") chybi");
						return false;
					}

					if(input.UTXO.value != tempOutput.value) {
						System.out.println("#Vstupy transakce (" + t + ") jsou neplatne");
						return false;
					}

					tempUTXOs.remove(input.transactionOutputId);
				}

				for(TransactionOutput output: currentTransaction.outputs) {
					tempUTXOs.put(output.id, output);
				}

				if( currentTransaction.outputs.get(0).reciepient != currentTransaction.reciepient) {
					System.out.println("#Prijemce transakce (" + t + ") není ten, kdo by mel spravne byt (podvrh)");
					return false;
				}
				if( currentTransaction.outputs.get(1).reciepient != currentTransaction.sender) {
					System.out.println("#Hodnota Output sender u transakce (" + t + ") nesedi.");
					return false;
				}

			}

		}
		System.out.println("Blockchain je platny");
		return true;
	}

	public static void addBlock(Block newBlock) {
		newBlock.mineBlock(difficulty);
		blockchain.add(newBlock);
	}
}
