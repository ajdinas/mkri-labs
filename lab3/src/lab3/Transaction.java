package lab3;

import java.security.*;
import java.util.ArrayList;

public class Transaction {

    public String transactionId; // taktez hash transakce.
    public PublicKey sender; // adresa odesilatele/verejny klic
    public PublicKey reciepient; // adresa prijece/verejny klic
    public float value;
    public byte[] signature; // podpis zabrani komukoli cizimu  odesilat platby

    public ArrayList<TransactionInput> inputs = new ArrayList<TransactionInput>();
    public ArrayList<TransactionOutput> outputs = new ArrayList<TransactionOutput>();

    private static int sequence = 0; // pocitadlo vygenerovanych transakci

    // Constructor:
    public Transaction(PublicKey from, PublicKey to, float value,  ArrayList<TransactionInput> inputs) {
        this.sender = from;
        this.reciepient = to;
        this.value = value;
        this.inputs = inputs;
    }

    // Pocita hash transakce, bude pouzit jako jeji ID
    private String calulateHash() {
        sequence++; //ikrementace sekvence, aby nahodou dve transakce nemeli stejny hash
        return StringUtil.applySha256(
                StringUtil.getStringFromKey(sender) +
                        StringUtil.getStringFromKey(reciepient) +
                        Float.toString(value) + sequence
        );
    }

    // Podepise data, ktere chceme ochranit před manipulaci.
    public void generateSignature(PrivateKey privateKey) {
        String data = StringUtil.getStringFromKey(sender) + StringUtil.getStringFromKey(reciepient) + Float.toString(value)	;
        signature = StringUtil.applyECDSASig(privateKey,data);
    }
    // Overi, ze s podepsanymi daty nikdo cizi nemanipuloval
    public boolean verifiySignature() {
        String data = StringUtil.getStringFromKey(sender) + StringUtil.getStringFromKey(reciepient) + Float.toString(value)	;
        return StringUtil.verifyECDSASig(sender, data, signature);
    }

    // vrati true, pokud by mohla byt vytvorena nova transakce
    public boolean processTransaction() {

        if(verifiySignature() == false) {
            System.out.println("#Selhalo overeni podpisu transakce");
            return false;
        }

        //shromazduje vstupy transakci (ujistit se, ze jsou nevycerpane):
        for(TransactionInput i : inputs) {
            i.UTXO = Blockchain.UTXOs.get(i.transactionOutputId);
        }

        // overeni platnosti transakce:
        if(getInputsValue() < Blockchain.minimumTransaction) {
            System.out.println("#Vstupy transakce jsou priliz male: " + getInputsValue());
            return false;
        }

        //generuje vystup transakce:
        float leftOver = getInputsValue() - value;
        transactionId = calulateHash();
        //odesle castku prijemci
        outputs.add(new TransactionOutput( this.reciepient, value,transactionId));
        outputs.add(new TransactionOutput( this.sender, leftOver,transactionId)); //Odesle zpet odesilatelovi
        //prida vystup na seznam neutracenych transakci
        for(TransactionOutput o : outputs) {
            Blockchain.UTXOs.put(o.id , o);
        }

        //odecte Inputs ze seznamu UTXO:
        for(TransactionInput i : inputs) {
            if(i.UTXO == null) continue; //Pokud nelze transakci nalezt, preskoci ji
            Blockchain.UTXOs.remove(i.UTXO.id);
        }

        return true;
    }

    //vraci sumu inputs(UTXOs)
    public float getInputsValue() {
        float total = 0;
        for(TransactionInput i : inputs) {
            if(i.UTXO == null) continue; // Pokud nelze transakci nalezt, preskoci ji
            total += i.UTXO.value;
        }
        return total;
    }

    //returns sum of outputs:
    public float getOutputsValue() {
        float total = 0;
        for(TransactionOutput o : outputs) {
            total += o.value;
        }
        return total;
    }
}
