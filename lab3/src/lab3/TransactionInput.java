package lab3;

public class TransactionInput {
    public String transactionOutputId; //Odkaz na TransactionOutputs -> transactionId
    public TransactionOutput UTXO; //Obsahuje Output neutracené transakce

    public TransactionInput(String transactionOutputId) {
        this.transactionOutputId = transactionOutputId;
    }
}
