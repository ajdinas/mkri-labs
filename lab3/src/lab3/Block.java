package lab3;

import java.util.ArrayList;
import java.util.Date;

public class Block {

	public String hash;
	public String previousHash;
	public String merkleRoot;
	public ArrayList<Transaction> transactions = new ArrayList<Transaction>();
	public long timeStamp; //pocet milisekund od 1.1.1970
	public int nonce;

	//Block Constructor.
	public Block(String previousHash ) {
		this.previousHash = previousHash;
		this.timeStamp = new Date().getTime();

		this.hash = calculateHash();
	}

	//Spocita novy hash na zaklade obsahu v bloku
	public String calculateHash() {
		String calculatedhash = StringUtil.applySha256(
				previousHash +
						Long.toString(timeStamp) +
						Integer.toString(nonce) +
						merkleRoot
		);
		return calculatedhash;
	}

	//Zvysuje hodnotu Nonce dokud nenajde pozadovany hash.
	public void mineBlock(int difficulty) {
		merkleRoot = StringUtil.getMerkleRoot(transactions);
		String target = StringUtil.getDificultyString(difficulty); //// Vytvori string s obtiznosti * "0"
		while(!hash.substring( 0, difficulty).equals(target)) {
			nonce ++;
			hash = calculateHash();
		}
		System.out.println("Blok vytezen!!! : " + hash);
	}

	//Prida transakce do tohoto bloku
	public boolean addTransaction(Transaction transaction) {
		//zpracuje transakci a overi ji. pokud jde o pocatecni blok, ignoruje to.
		if(transaction == null) return false;
		if((previousHash != "0")) {
			if((transaction.processTransaction() != true)) {
				System.out.println("Error zpracovani transakce.");
				return false;
			}
		}
		transactions.add(transaction);
		System.out.println("Transakce byla uspesne pridana do bloku");
		return true;
	}

}
