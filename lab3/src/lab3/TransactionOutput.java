package lab3;

import java.security.PublicKey;

public class TransactionOutput {
    public String id;
    public PublicKey reciepient; //novy majitel minci
    public float value; //castka, kterou vlastni
    public String parentTransactionId; //id transakce, ve které by tento output vytvoren

    //Constructor
    public TransactionOutput(PublicKey reciepient, float value, String parentTransactionId) {
        this.reciepient = reciepient;
        this.value = value;
        this.parentTransactionId = parentTransactionId;
        this.id = StringUtil.applySha256(StringUtil.getStringFromKey(reciepient)+Float.toString(value)+parentTransactionId);
    }

    //kontrola, zda mince patri vam
    public boolean isMine(PublicKey publicKey) {
        return (publicKey == reciepient);
    }

}
