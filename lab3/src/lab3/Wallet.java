package lab3;

import java.security.*;
import java.security.spec.ECGenParameterSpec;
import java.util.*;

public class Wallet {

    public PrivateKey privateKey;
    public PublicKey publicKey;

    public HashMap<String,TransactionOutput> UTXOs = new HashMap<String,TransactionOutput>(); //pouze UTXO vlastnene touto penezenkou


    public Wallet(){
        generateKeyPair();
    }

    public void generateKeyPair() {
        try {
            KeyPairGenerator keyGen = KeyPairGenerator .getInstance("ECDSA","BC");
            SecureRandom random = SecureRandom .getInstance("SHA1PRNG");
            ECGenParameterSpec ecSpec = new ECGenParameterSpec("prime192v1");
            // Inicializace keypair generatoru a generovani
            keyGen.initialize(ecSpec, random);  //256 bytů poskytuje dostatecnou uroven zabezpeceni
            KeyPair keyPair = keyGen.generateKeyPair();
            // Stanoveni soukromého a veřejného klice
            privateKey = keyPair.getPrivate();
            publicKey = keyPair.getPublic();
        }catch(Exception e) {
            throw new RuntimeException(e);
        }
    }

    // vraci bilanci a uklada UTXO vlastnene touto penezenkou
    public float getBalance() {
        float total = 0;
        for (Map.Entry<String, TransactionOutput> item: Blockchain.UTXOs.entrySet()){
            TransactionOutput UTXO = item.getValue();
            if(UTXO.isMine(publicKey)) { //pokuc output (mince) patri mne
                UTXOs.put(UTXO.id,UTXO); //Prida na seznam neutracenych transakci
                total += UTXO.value ;
            }
        }
        return total;
    }

    //generuje a vraci novou transakci z této penezenky.
    public Transaction sendFunds(PublicKey _recipient,float value ) {
        if(getBalance() < value) { //ziska bilanci a zkontroluje castku.
            System.out.println("#Nedostatek minci k odeslani transakce. Transakce zrusena.");
            return null;
        }
        //vytvori array list vstupu
        ArrayList<TransactionInput> inputs = new ArrayList<TransactionInput>();

        float total = 0;
        for (Map.Entry<String, TransactionOutput> item: UTXOs.entrySet()){
            TransactionOutput UTXO = item.getValue();
            total += UTXO.value;
            inputs.add(new TransactionInput(UTXO.id));
            if(total > value) break;
        }

        Transaction newTransaction = new Transaction(publicKey, _recipient , value, inputs);
        newTransaction.generateSignature(privateKey);

        for(TransactionInput input: inputs){
            UTXOs.remove(input.transactionOutputId);
        }
        return newTransaction;
    }
}